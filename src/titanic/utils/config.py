import argparse
import typing as ty
import yaml


def read_config() -> ty.Dict[str, ty.Any]:
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", type=str, default="params.yaml")
    args = parser.parse_args()
    with open(args.config) as file:
        return yaml.full_load(file)

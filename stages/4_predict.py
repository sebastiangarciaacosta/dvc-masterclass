from titanic.utils import read_config
import os.path as osp
import logging
import pickle as pkl

import pandas as pd

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def main(params) -> None:
    cfg = params["predict"]
    target = params["train"]["target"]

    logger.info(f"Loading model from {cfg['inputs']['model']}")
    with open(cfg["inputs"]["model"], "rb") as f:
        model = pkl.load(f)

    for dataset, filename in cfg["inputs"]["data"]["files"].items():
        path = osp.join(cfg["inputs"]["data"]["dir"], filename)
        logger.info(f"Reading {dataset} data from {path}")
        data = pd.read_parquet(path)
        logger.info(f"Generating predictions...")
        features = data.drop(columns=[target])
        scores = model.predict_proba(features)[:, 1]
        preds_df = pd.DataFrame(dict(score=scores), index=data.index)
        if target in data.columns:
            preds_df[target] = data[target]
        save_path = osp.join(cfg["outputs"]["dir"], f"{dataset}.parquet")
        logger.info(f"Saving predictions to {save_path}")
        preds_df.to_parquet(save_path)
        logger.info("Done!\n")


if __name__ == "__main__":
    params = read_config()
    main(params)

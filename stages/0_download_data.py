from titanic.utils import read_config
import os
import requests
import logging

logging.basicConfig()
logger = logging.getLogger(__name__)


if __name__ == "__main__":
    params = read_config()
    logger.setLevel(params["base"]["log_level"])
    cfg = params["download_data"]

    for url in cfg["urls"]:
        name = url.split("/")[-1]
        path = os.path.join(cfg["output"], name)
        logger.info(f"Performing GET request to: {url}")
        response = requests.get(url)
        logger.info(f"Status code: {response.status_code}")
        if response.status_code != 200:
            logger.warn(f"Status code is {response.status_code}, skipping")
            continue
        logger.info(f"Saving data to {path}...")
        with open(path, "wb") as f:
            f.write(response.content)
        logger.info("Data saved succesfully")

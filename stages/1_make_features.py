from titanic.utils import read_config

# Python built-in libraries
import typing as ty
import logging
import os.path as osp
import pickle

# External libraries
import pandas as pd
from sklearn.preprocessing import OneHotEncoder

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def make_features(
    data: pd.DataFrame,
    cat_features: ty.List[str],
    one_hot_encoder: ty.Optional[OneHotEncoder] = None,
    train: bool = False,
) -> pd.DataFrame:
    """
    Reads raw data and performs one-hot encoding on categorical features.
    Arguments:
        train: training data
        test: test data
        output_transformed_dir: str, directory of the transformed data.
        cat_features: List[str]: list of names of categorical features.
    Returns:
        train: one-hot encodded train set
        test: one-hot encodded test set
    """
    logger.info("One hot encoding...")
    if train:
        one_hot_encoder.fit(data[cat_features])
    encoded = one_hot_encoder.transform(data[cat_features])
    cat_encoded = pd.DataFrame(
        encoded.toarray(), columns=one_hot_encoder.get_feature_names_out()
    )
    logger.info(f"One hot encoded categories: {cat_encoded.columns.tolist()}")
    data.drop(columns=cat_features, inplace=True)
    data = pd.concat([data, cat_encoded], axis=1)
    return data


def main(params):
    logger.debug("Reading params file...")
    cfg = params["make_features"]
    cat_feat = params["base"]["features"]["categorical"]

    logger.info("Reading input files...")
    ohe_out_path = osp.join(cfg["output"], "one_hot_encoder.pkl")

    if "train" in cfg["inputs"]["files"].keys():
        logger.info("Reading training data...")
        input_path = osp.join(cfg["inputs"]["dir"], cfg["inputs"]["files"]["train"])
        train_df = pd.read_csv(input_path)
        ohe = OneHotEncoder(handle_unknown="ignore")

        train_df = make_features(
            train_df,
            cat_feat,
            ohe,
            train=True,
        )

        logger.info("Intermediate features shape: {}".format(train_df.shape))
        out_path = osp.join(cfg["output"], "train.parquet")

        logger.info("Saving intermediate features to {}".format(out_path))
        train_df.to_parquet(out_path)

        logger.info("Saving one-hot encoder to {}".format(ohe_out_path))
        with open(ohe_out_path, "wb") as f:
            pickle.dump(ohe, f)

        del cfg["inputs"]["files"]["train"]
    else:
        with open(ohe_out_path, "rb") as f:
            ohe = pickle.load(f)

    for dataset, filename in cfg["inputs"]["files"].items():
        path = osp.join(cfg["inputs"]["dir"], filename)
        logger.info(f"Reading: {dataset} set from {path}")
        data = pd.read_csv(path)
        data = make_features(data, cat_feat, one_hot_encoder=ohe)
        out_path = osp.join(cfg["output"], f"{dataset}.parquet")
        logger.info(f"Saving to {out_path}")
        data.to_parquet(out_path)


if __name__ == "__main__":
    params = read_config()
    logger.setLevel(params["base"]["log_level"])
    main(params)

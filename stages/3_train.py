from titanic.utils import import_name, read_config
import os.path as osp
import logging
import pickle as pkl
import pandas as pd

from sklearn.model_selection import GridSearchCV

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def main(params):
    cfg = params["train"]
    logger.info("Reading processed training set...")
    train = pd.read_parquet(cfg["input"])
    X, y = train.drop(columns=cfg["target"]), train[cfg["target"]]

    logger.info(
        f'Instantiating model from: {cfg["model"]["module"]}.{cfg["model"]["name"]}'
    )
    model_cls = import_name(cfg["model"]["module"], cfg["model"]["name"])
    if "kwargs" in cfg["model"]:
        model = model_cls(**cfg["model"]["kwargs"])
    else: 
        model = model_cls()

    logger.info(f"Instantiating grid search with kwargs: {cfg['grid_search_cv']}")
    clf: GridSearchCV = GridSearchCV(model, **cfg["grid_search_cv"])

    logger.info("Training model...")
    clf.fit(X, y)

    results = pd.DataFrame(clf.cv_results_)
    results.sort_values(by="rank_test_score", inplace=True)
    logger.info(f"Results: {results}")
    logger.info(f"Best {clf.scorer_}: {clf.best_score_}")
    logger.info(f"Saving results to {cfg['outputs']['results']}")
    results.to_csv(cfg["outputs"]["results"], index=False)

    save_path = osp.join(cfg["outputs"]["model"])
    logger.info(f"Done, saving best model found to {save_path}")
    with open(save_path, "wb") as f:
        pkl.dump(clf.best_estimator_, f)
    logger.info("Done")


if __name__ == "__main__":
    params = read_config()
    main(params)

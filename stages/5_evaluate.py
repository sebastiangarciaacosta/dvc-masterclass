from titanic.utils import read_config

import os
import logging
import typing as ty

import numpy as np
import pandas as pd
import wandb
from titanic.utils import import_name

logger = logging.getLogger(__name__)


def main(params):
    cfg = params["evaluate"]
    wandb.login()
    wandb.init(config=params, **cfg.pop("wandb"))

    target = params["train"]["target"]
    inputs = cfg["inputs"]["files"]
    paths: ty.List[ty.Tuple[str, str]] = []

    if "train" in inputs:
        paths.append(("train", os.path.join(cfg["inputs"]["dir"], inputs["train"])))
        del inputs["train"]

    for dataset, filename in inputs.items():
        path = os.path.join(cfg["inputs"]["dir"], filename)
        paths.append((dataset, path))

    cv_results = pd.read_csv(params["train"]["outputs"]["results"])
    cv_results_table = wandb.Table(dataframe=cv_results)
    results: ty.Dict[str, ty.Any] = {"cv_results": cv_results_table}

    for dataset, path in paths:
        print("Evaluating on {}".format(dataset))
        preds_df = pd.read_parquet(path)
        if not target in preds_df.columns:
            print(f"Skipping {dataset} because does not have target column {target}")
            continue

        scores = np.zeros((preds_df.shape[0], 2))
        scores[:, 1] = preds_df["score"]
        scores[:, 0] = 1 - preds_df["score"]
        labels = [f"not {target}", target]

        print("Plotting ROC curve...")
        roc_plot = wandb.plot.roc_curve(
            preds_df[target],
            scores,
            labels=labels,
            title=f"{dataset} ROC curve",
            classes_to_plot=[1],
        )
        results[f"{dataset}_roc"] = roc_plot

        print("Plotting PR curve...")
        pr_curve = wandb.plot.pr_curve(
            preds_df[target],
            scores,
            labels=labels,
            title=f"{dataset} PR curve",
            classes_to_plot=[1],
        )
        results[f"{dataset}_pr"] = pr_curve

        for metric, args in cfg["metrics"].items():
            score_fn = import_name(args["module"], args["name"])
            metric_result = score_fn(preds_df[target], preds_df["score"])
            kv = "\t{} = {}".format(metric, metric_result)
            logger.info(kv)
            print(kv)
            results[f"{dataset}_{metric}"] = metric_result

        for i, c in enumerate(cv_results.columns.tolist()):
            results[c] = cv_results.iloc[0, i]

        logger.info("\n")
        print("\n")
    wandb.log(results, commit=True)


if __name__ == "__main__":
    params = read_config()
    logger.setLevel(params["base"]["log_level"])
    main(params)

from titanic.utils import read_config, import_name
import typing as ty
import os.path as osp
import logging

from sklearn.preprocessing import StandardScaler
import pandas as pd
import pickle
import yaml

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def main(params):
    logger.info("Reading params file...")

    cfg = params["process"]
    cont_feat = params["base"]["features"]["continuous"]

    tmodule, tname = cfg["transformer"]["module"], cfg["transformer"]["name"]
    transformer_out_path = osp.join(cfg["inputs"]["dir"], "transformer.pkl")

    logger.info("Reading input files...")

    if "train" in cfg["inputs"]["files"].keys():
        logger.info("Reading training data...")
        input_path = osp.join(cfg["inputs"]["dir"], cfg["inputs"]["files"]["train"])
        train_df = pd.read_parquet(input_path)
        transformer_cls = import_name(tmodule, tname)
        if "kwargs" in cfg["transformer"]:
            transformer = transformer_cls(**cfg["transformer"]["kwargs"])
        else:
            transformer = transformer_cls()

        logger.info("Fitting scaler...")
        transformer.fit(train_df[cont_feat])

        logger.info(
            f"Done, saving {tmodule}.{tname} to transformation zone at {transformer_out_path}"
        )
        with open(transformer_out_path, "wb") as f:
            pickle.dump(transformer, f)

        logger.info("Saved succesefully")
    else:
        logger.info("Did not find training data, reading saved scaler")
        with open(transformer_out_path, "rb") as f:
            transformer = pickle.load(f)

    logger.info("Transforming datasets...")

    for dataset, filename in cfg["inputs"]["files"].items():
        path = osp.join(cfg["inputs"]["dir"], filename)
        logger.info(f"Reading: {dataset} set from {path}")
        data = pd.read_parquet(path)
        data[cont_feat] = transformer.transform(data[cont_feat])
        out_path = osp.join(cfg["output"], f"{dataset}.parquet")

        logger.info(f"Done, saving {dataset} to feature zone at {out_path}")
        data.to_parquet(out_path)


if __name__ == "__main__":
    params = read_config()
    main(params)

# DVC masterclass

# Project requirements

- Python 3.8
- conda 4.10.3
- pip 22.0.4

# Get started

## Create project structure

```
# Clone the repository
git clone git@gitlab.com:sebastiangarciaacosta/dvc-masterclass.git 
# Enters the project folder
cd dvc-masterclass
```

## Create virtual environment
 
Creating a virtual environment from scratch and then installing libraries:

```
conda create -n dvc-env python=3.8
conda activate dvc-env
pip install --no-cache-dir -r dependencies/requirements.txt
pip install -e .
```

or creating an environment with the exported libraries of the conda environment.

```
conda env create --file dependencies/environment.yaml 
```

# Development setup
```bash
# Generates dependencies/requirements.txt file with fixed compatible versions for each library
pip-compile dependencies/requirements.in --allow-unsafe
# Generates dependencies/dev_requirements.txt file with fixed compatible versions for each library
pip-compile dependencies/dev_requirements.in --allow-unsafe
# Creates the pyproject.toml file
poetry init
# Adds dependencies to pyproject.toml file
poetry add $( cat dependencies/requirements.txt | grep -E '^[^# ][^- ]' ) 
# Adds development dependencies to pyproject.toml file
poetry add -D $( cat dependencies/dev_requirements.txt | grep -E '^[^# ][^- ]' )
# Install requirements.txt in order to update pip to 22.0.4
pip install --no-cache-dir -r dependencies/requirements.txt
# Install library in development mode (-e)
pip install -e .
```

# Pipeline
```
git checkout develop
dvc repro
```

## Git workflow

```bash
# Create local branch
git checkout -b backtesting/<initials>-<model-type>
# Code training and predict script ...
# Marge requrest to develop branch
git push origin backtesting/<initials>-<model-type> : develop  
```

# DVC

These commands are used to intialize DVC on the project

```
dvc init
dvc remote add --default storage gdrive://<folder id>
dvc add data models reports
git add data.dvc models.dvc reports.dvc
dvc push
git push origin main
```
